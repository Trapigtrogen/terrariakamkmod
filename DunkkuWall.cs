﻿using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace OfficialKAMKMod
{
    class DunkkuWall : ModWall
    {
        public override void SetDefaults()
        {
            Main.wallHouse[Type] = true;
            dustType = mod.DustType("Sparkle");
            drop = mod.ItemType("DunkkuWallItem");
            AddMapEntry(new Color(112, 108, 64));
        }
    }
}
