﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using static Terraria.ModLoader.ModContent;

namespace OfficialKAMKMod
{
    class DunkkuTileItem : ModItem
    {
        public override void SetStaticDefaults()
        {
            Tooltip.SetDefault("Dunkku tiili.");
        }

        public override void SetDefaults()
        {
            item.width = 20;
            item.height = 20;
            item.maxStack = 999;
            item.value = 100;
            item.rare = 1;
            item.useTime = 10;
            item.useAnimation = 15;
            item.autoReuse = true;
            item.createTile = TileType<DunkkuTile>();
            item.consumable = true;
            item.useStyle = 1;
            //item.placeStyle = 1;
            // Set other item.X values here
        }
    }
}
