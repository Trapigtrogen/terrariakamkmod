﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.World.Generation;
using Terraria.GameContent.Generation;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using static Terraria.ModLoader.ModContent;
using System.Diagnostics;
using System;
using System.Reflection;

namespace OfficialKAMKMod
{
    struct Vec2
    {
        public int x, y;
    };

    public class DunkkuGen : ModWorld
    {
        public static int biomeTiles = 0;

        const int width = 600;
        const int height = 600;
        int numCaveMin;         //min number of caves
        int numCaveMax;         //max number of caves
        int sizeCaveMin;        //cave brush min size
        int sizeCaveMax;        //cave brush max size
        int numCaveNodeMin;     //node min count
        int numCaveNodeMax;     //node max count
        int lenNodeDistMin;     //min node distance
        int lenNodeDistMax;     //max node distance
        int sizeConnectMin;     //cave connector brush min size
        int sizeConnectMax;     //cave connector brush max size
        int lenConnectDistMin;  //min connector node distance
        int lenConnectDistMax;  //max connector node distance
        int maxConnectStep;     //max number of steps connector will take
        int distConnect;        //distance from connector node to cave node in which the connector considers the caves connected
        float biasConnect;      //connector bias to travel in a straight line
        float crossConnect;     //chance for random connections
        float caveVal;          //how straight will caves be
        float drawRange;        //changes fill algorithm result. 1 is default
        int cleanIteration;     //how many cleaning iterations
        int clearOuterIter;     //how many passes to add the wall after clearing. final value is this +1
        int distCaveMin;        //minimum distance of caves

        int[,] map = new int[width,height];
        int[,] mapt = new int[width, height];
        int[,] mapt2 = new int[width, height];

        public override void ModifyWorldGenTasks(List<GenPass> tasks, ref float totalWeight)
        {
            /*foreach (GenPass gen in tasks)
            {
                Debug.WriteLine("Task: " + gen.Name);
            }*/

            int genIndex = tasks.FindIndex(genpass => genpass.Name.Equals("Lakes"));
            

            if (genIndex != -1)
            {
                tasks.Insert(genIndex + 1, new PassLegacy("Dunkku Generatio", DunkkuGenF));
            }
        }

        private void DunkkuGenF(GenerationProgress progress)
        {
            progress.Message = "Dunnkua ollaan tekemässä. Odotappa täsä. :--DDDD";

            int wX = WorldGen.genRand.Next(width/2 + Main.maxTilesX/3, Main.maxTilesX-width/2 - Main.maxTilesX/3);
            int wY = WorldGen.genRand.Next((int)WorldGen.worldSurfaceHigh + height/2, Main.maxTilesY-height/2 - (Main.maxTilesY - (Main.maxTilesY == 2400 ? 2240 : Main.maxTilesY == 1900 ? 1540 : 1020)));
            //Debug.WriteLine("Right: " + Main.rightWorld.ToString());
            //Debug.WriteLine("Left: " + Main.leftWorld.ToString());
            //Debug.WriteLine("wsurface; " + WorldGen.worldSurface.ToString());
            //Debug.WriteLine("wsurfaceh; " + WorldGen.worldSurfaceHigh.ToString());
            //Debug.WriteLine("wsurfacel; " + WorldGen.worldSurfaceLow.ToString());

            //int X = WorldGen.genRand.Next(0, width);
            //int Y = WorldGen.genRand.Next(/*(int)WorldGen.worldSurfaceHigh*/0, height);

            numCaveMin = 5;         
            numCaveMax = 8;         
            sizeCaveMin = 3;       
            sizeCaveMax = 6;       
            numCaveNodeMin = 2;     
            numCaveNodeMax = 7;     
            lenNodeDistMin = 10;    
            lenNodeDistMax = 20;    
            sizeConnectMin = 3;     
            sizeConnectMax = 6;    
            lenConnectDistMin = 15; 
            lenConnectDistMax = 30; 
            maxConnectStep = 300;   
            distConnect = 8;        
            biasConnect = 0.7f;     
            crossConnect = 0.3f;    
            caveVal = 0.0f;         
            drawRange = 1.0f;       
            cleanIteration = 3;
            clearOuterIter = 15;
            distCaveMin = 50;

            Generate ( 100, 100, width - 100, height - 100, wX, wY ) ;
        }

        void Generate(int x1, int y1, int x2, int y2, int wX, int wY)
        {
            int numCave = WorldGen.genRand.Next(numCaveMin, numCaveMax); //Get number of caves to make
            //Vec2** caves = calloc(numCave, sizeof(Vec2*));
            List<List<Vec2>> caves = new List<List<Vec2>>(numCave);
            //int* numNodes = calloc(numCave, sizeof(int));
            List<int> numNodes = new List<int>(numCave);

            //make caves
            for (int i = 0; i < numCave; i++)
            {
                numNodes.Add(WorldGen.genRand.Next(numCaveNodeMin, numCaveNodeMax));  //get number of nodes for cave
                caves.Add(new List<Vec2>(numNodes[i])); // calloc(numNodes[i], sizeof(Vec2));
                Vec2 cpos = new Vec2();

                for (int co = 0; co < 10; co++)
                {
                    cpos = new Vec2
                    {
                        x = WorldGen.genRand.Next(x1, x2),    //Get cave origin point
                        y = WorldGen.genRand.Next(y1, y2)
                    };

                    if (co == 9) break;

                    bool exit = true;
                    for(int ac = 0; ac < i; ac++)
                    {
                        if (distCaveMin < Math.Sqrt(Math.Pow(Math.Abs(cpos.x - caves[ac][0].x), 2) + Math.Pow(Math.Abs(cpos.y - caves[ac][0].y), 2)))
                        {
                            exit = false;
                            break;
                        }
                    }
                    if (exit) break;
                }

                caves[i].Add(cpos);

                float angle = 10;

                for (int j = 1; j < numNodes[i]; j++)
                {
                    //Get node point
                    float nangle;
                    nangle = (angle == 10) ? WorldGen.genRand.Next(0, 360) * (float)Math.PI / 180.0f :
                    angle + (float)( WorldGen.genRand.Next(0, 1).Equals(1) ? -1 : 1) * ( WorldGen.genRand.Next(0, (int)(caveVal * 180f))) * (float)Math.PI / 180.0f;
                    angle = nangle;
                    
                    caves[i].Add(new Vec2
                    {
                        x = caves[i][j - 1].x + (int)(Math.Sin(angle) * (double)WorldGen.genRand.Next(lenNodeDistMin, lenNodeDistMax)),
                        y = caves[i][j - 1].y + (int)(Math.Cos(angle) * (double)WorldGen.genRand.Next(lenNodeDistMin, lenNodeDistMax))
                    });
                }
            }

            int numConnector = 0;
            //int* numConnectorNode = malloc(1);
            List<int> numConnectorNode = new List<int>();
            //Vec2** connectors = malloc(1);
            List<List<Vec2>> connectors = new List<List<Vec2>>();
            int cIndex = numCave - 1;


            //make connectors
            for (int i = 0; i < numCave; i++)
            {
                int cn1 =  WorldGen.genRand.Next(0, numNodes[i] - 1);      //get random node from cave[i]
                int cn2 =  WorldGen.genRand.Next(0, numNodes[cIndex] - 1); //get random node from previous cave

                //connectors = realloc(connectors, (++numConnector) * sizeof(Vec2*));
                connectors.Capacity = ++numConnector;
                //numConnectorNode = realloc(numConnectorNode, numConnector * sizeof(int));
                numConnectorNode.Capacity = numConnector;
                connectors.Add(Connect(caves[i][cn1], caves[cIndex][cn2])); //call connect
                //connectors[numConnector - 1] = new List<Vec2>()

                if ( WorldGen.genRand.Next(0, 100) >= (int)(crossConnect * 100)) //random change for another connection to a random cave
                {
                    int oc = i;
                    while (oc == i) { oc =  WorldGen.genRand.Next(0, numCave - 1); }

                    cn1 =  WorldGen.genRand.Next(0, numNodes[i] - 1);
                    cn2 =  WorldGen.genRand.Next(0, numNodes[oc] - 1);

                    //connectors = realloc(connectors, (++numConnector) * sizeof(Vec2*));
                    connectors.Capacity = ++numConnector;
                    //numConnectorNode = realloc(numConnectorNode, numConnector * sizeof(int));
                    numConnectorNode.Capacity = numConnector;
                    connectors.Add(Connect(caves[i][cn1], caves[oc][cn2]));
                }
                cIndex = i;
            }

            map.Populate(0);
            mapt.Populate(0);
            mapt2.Populate(0);
            for (int i = 0; i < numCave; i++) { 
                Fill(caves[i], numNodes[i], sizeCaveMin, sizeCaveMax);}
            for (int i = 0; i < numConnector; i++)
                Fill(connectors[i], connectors[i].Count, sizeConnectMin, sizeConnectMax);
            for (int i = 0; i < cleanIteration; i++)
                Clean();
            ClearWalls();
            Show(wX,wY);
        }

        List<Vec2> Connect(Vec2 p1, Vec2 p2)
        {
            //Vec2* cnodes = malloc(1);
            List<Vec2> cnodes = new List<Vec2>();
            int numNodes = 0;
            Vec2 point = p1;

            for (int i = 0; i < maxConnectStep; i++) //try to step from p1 to p2, but stop after max steps to prevent infinite loop (if that's even possible in the first place)
            {
                //cnodes = realloc(cnodes, (1 + numNodes) * sizeof(Vec2));
                cnodes.Capacity = 1 + numNodes;

                int cd =  WorldGen.genRand.Next(0, 1).Equals(1) ? -1 : 1;  //decide direction of angle deviation

                float angle = -((float)Math.Atan2((float)(p2.x - point.x), (float)(point.y - p2.y))) + (float)Math.PI; //get angle from point to p2
                angle = angle + (float)cd * ( WorldGen.genRand.Next(0, (int)(biasConnect * 180))) * (float)Math.PI / 180.0f; //deviate angle according to bias

                int len =  WorldGen.genRand.Next(lenConnectDistMin, lenConnectDistMax);  //get radius
               
                cnodes.Add(new Vec2
                {
                    x = point.x + (int)(Math.Sin(angle) * len),       //get point on the circle
                    y = point.y + (int)(Math.Cos(angle) * len)
                });

                point = cnodes[numNodes];

                numNodes++;
                if (Math.Sqrt(Math.Pow(p2.x - point.x, 2) + Math.Pow(p2.y - point.y, 2)) <= distConnect) //if close enough to to p2, stop
                    break;
            }
            return cnodes;
        }

        float absf(float value) { return (value < 0) ? -value : value; }
        int clampi(int x, int min, int max)
        {
            if (x < min) return min;
            if (x > max) return max;
            return x;
        }
        

        void DrawLineMap(int x1, int y1, int x2, int y2)
        {
            x1 = clampi(x1, 0, width - 1);
            x2 = clampi(x2, 0, width - 1);
            y1 = clampi(y1, 0, height - 1);
            y2 = clampi(y2, 0, height - 1);

            int dx = Math.Abs(x2 - x1), sx = x1 < x2 ? 1 : -1;
            int dy = Math.Abs(y2 - y1), sy = y1 < y2 ? 1 : -1;
            int err = (dx > dy ? dx : -dy) / 2, e2;

            while (true)
            {
                mapt[x1,y1] = 1;

                if (x1 == x2 && y1 == y2)
                {
                    break;
                }

                e2 = err;

                if (e2 > -dx)
                {
                    err -= dy;
                    x1 += sx;
                }
                if (e2 < dy)
                {
                    err += dx;
                    y1 += sy;
                }
            }
        }

        void Fill(List<Vec2> nodes, int numNodes, int bmin, int bmax)
        {

            for (int i = 1; i < numNodes; i++)
            {
                int bs =  WorldGen.genRand.Next(bmin, bmax);     //get brush radius

                float angle = -((float)Math.Atan2((float)(nodes[i].x - nodes[i - 1].x - 0.5f), (float)(nodes[i - 1].y - nodes[i].y - 0.5f))) + (float)Math.PI / 2.0f;  //get angle 90deg from the angle between points

                float anglestep = 2 * (float)Math.Asin(1.0f / (2.0f * (float)(bs * drawRange)));     //get angle step for 1 pixel 
                int steps = (int)Math.Ceiling(absf(180.0f / (anglestep * 180.0f / (float)Math.PI)));    //get number of steps for 180deg

                for (int j = 0; j <= steps; j++)
                {
                    //DrawLine(nodes[i].x + sinf(angle + anglestep*j) * bs,nodes[i].y + cosf(angle + anglestep*j) * bs,nodes[i-1].x + sinf(angle - anglestep*j) * bs, nodes[i-1].y + cosf(angle - anglestep*j) * bs, w_Rgba(255,255,255,255));
                    DrawLineMap(nodes[i].x + (int)(Math.Sin(angle + anglestep * j) * bs), nodes[i].y + (int)(Math.Cos(angle + anglestep * j) * bs), nodes[i - 1].x + (int)(Math.Sin(angle - anglestep * j) * bs), nodes[i - 1].y + (int)(Math.Cos(angle - anglestep * j) * bs));
                }
            }
        }

        int GetNCount(int x, int y)
        {
            int c = 0;
            if (y != 0 && mapt2[x,y-1] == 1) c++;
            if (y != height-1 && mapt2[x,y+1] == 1) c++;
            if (x != 0 && mapt2[x-1,y] == 1) c++;
            if (x != width-1 && mapt2[x+1,y] == 1) c++;
            return c;
        }

        void Clean()
        {
            //memcpy(mapt2, mapt, width * height);
            Array.Copy(mapt, mapt2, mapt.Length);
            //memset(mapt, 0, width * height);
            mapt.Populate(0);

            for (int i = 0; i < mapt.GetLength(0); i++)
            {
                for (int j = 0; j < mapt.GetLength(1); j++)
                {
                    if (GetNCount(i, j) > 1 || mapt2[i, j] == 1)
                        mapt[i, j] = 1;
                }
            }
        }

        int GetNCountCW(int x, int y)
        {
            int c = 0;
            if (y != 0 && (mapt2[x,y] == 1 || mapt2[x,y-1] == 0)) c++;
            if (y != height-1 && (mapt2[x,y+1] == 1 || mapt2[x,y+1] == 0)) c++;
            if (x != 0 && (mapt2[x-1,y] == 1 || mapt2[x-1,y] == 0)) c++;
            if (x != width-1 && (mapt2[x+1,y] == 1 || mapt2[x+1,y] == 0)) c++;
            return c;
        }

        void ClearWalls()
        {
            //1st set all 0 to 2
            //memcpy(mapt2, mapt, width * height);
            //memset(mapt, 0, width * height);

            Array.Copy(mapt, mapt2, mapt.Length);
            mapt.Populate(0);

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (mapt2[i,j] == 0)
                        mapt[i,j] = 2;
                    else
                        mapt[i,j] = 1;
                }
            }

            //if tile is cleared and next to it is wall or cave, set to wall
            for (int x = 0; x < clearOuterIter + 1; x++)
            {
                //memcpy(mapt2, mapt, width * height);
                //memset(mapt, 0, width * height);

                Array.Copy(mapt, mapt2, mapt.Length);
                mapt.Populate(0);

                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        if (mapt2[i,j] == 2 && GetNCountCW(i, j) > 0)
                            mapt[i,j] = 0;
                        else if (mapt2[i,j] == 2)
                            mapt[i,j] = 2;
                        else if (mapt2[i,j] == 1)
                            mapt[i,j] = 1;
                    }
                }
            }
        }

        void AddLoot(int x1, int y1)
        {
            List<Vec2> floors = new List<Vec2>();

            for(int y = 0; y < height-1; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (map[x, y] == 1 && map[x, y + 1] == 0)
                    {
                        floors.Add(new Vec2{x = x,y = y});
                    }
                }
            }
            //Debug.WriteLine("floors:" + floors.Count.ToString());

            int min = 20, max = 30;
            int c = WorldGen.genRand.Next(min, max);

            //Debug.WriteLine("coins:" + c.ToString());

            for (int i = 0; i < c; i++)
            {
                Vec2 vp = floors[WorldGen.genRand.Next(0, floors.Count)];
                WorldGen.PlaceTile(x1 + vp.x - width / 2, y1 + vp.y - height / 2, TileID.PlatinumCoinPile, false, true);
            }
        }

        void AddWaterCandels(int x1, int y1)
        {
            List<Vec2> walls = new List<Vec2>();

            for (int y = 1; y < height - 1; y++)
            {
                for (int x = 1; x < width-1; x++)
                {
                    if (map[x, y] == 1 && (map[x-1, y] == 0 || map[x+1, y] == 0) && map[x, y - 1] == 1)
                    {
                        walls.Add(new Vec2 { x = x, y = y });
                    }
                }
            }
            //Debug.WriteLine("walls:" + walls.Count.ToString());

            int min = 70, max = 80;
            int c = WorldGen.genRand.Next(min, max);

            //Debug.WriteLine("candels:" + c.ToString());

            for (int i = 0; i < c; i++)
            {
                Vec2 vp = walls[WorldGen.genRand.Next(0, walls.Count)];
                WorldGen.PlaceTile(x1 + vp.x - width / 2, y1 + vp.y - height / 2, TileID.Platforms, false, true);
                WorldGen.PlaceTile(x1 + vp.x - width / 2, y1 + vp.y - 1 - height / 2, TileID.WaterCandle, false, true);
            }

        }

        void Show(int x1, int y1)
        {
            //memcpy(map, mapt, width * height);
            Array.Copy(mapt, map, mapt.Length);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (map[i,j] == 0)
                    {
                        //DrawPixel(i, j, w_Rgba(255, 255, 255, 255));
                        WorldGen.PlaceTile(x1 + i- width / 2, y1 + j - height / 2, mod.TileType("DunkkuTile"),false,true);
                        WorldGen.PlaceWall(x1 + i - width / 2, y1 + j - height / 2, mod.WallType("DunkkuWall"));
                        //Debug.WriteLine("X:" + i + ",Y:" + j);
                    }
                    else if(map[i, j] == 1)
                    {
                        WorldGen.PlaceTile(x1 + i - width / 2, y1 + j - height / 2, TileID.LavaDrip, false, true);
                        WorldGen.PlaceWall(x1 + i - width / 2, y1 + j - height / 2, mod.WallType("DunkkuWall"));
                    }
                    //WorldGen.PlaceTile(x1 + i - width / 2, y1 + j - height / 2, mod.TileType("DunkkuTile"), false, true);
                }
            }
            AddLoot(x1,y1);
            AddWaterCandels(x1,y1);
        }


        public override void TileCountsAvailable(int[] tileCounts)
        {
            biomeTiles = tileCounts[mod.TileType("DunkkuTile")];
        }

        public override void ResetNearbyTileEffects()
        {
            biomeTiles = 0;
        }
    }

    public static class Extensions
    {
        public static void Populate<T>(this T[,] arr, T value)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i, j] = value;
                }
            }
        }
    }
}

