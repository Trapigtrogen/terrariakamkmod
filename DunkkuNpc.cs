﻿using Terraria;
using Terraria.ModLoader;
using System.Diagnostics;

namespace OfficialKAMKMod
{
    class DunkkuNpc : GlobalNPC
    {
        static float mult = 15f;
        public override void EditSpawnRate(Player player, ref int spawnRate, ref int maxSpawns)
        {
            if (player.GetModPlayer<DunkkuPlayer>().zoneBiome)
            {
                spawnRate = (int)(spawnRate / mult);
                maxSpawns = (int)(maxSpawns * mult);
            }
        }

        public override void EditSpawnRange(Player player, ref int spawnRangeX, ref int spawnRangeY,
            ref int safeRangeX, ref int safeRangeY)
        {
            Debug.WriteLine("spawnrx: " + spawnRangeX.ToString());
            Debug.WriteLine("spawnry: " + spawnRangeY.ToString());

            Debug.WriteLine("spawnSrx: " + safeRangeX.ToString());
            Debug.WriteLine("spawnSry: " + safeRangeY.ToString());
            if (player.GetModPlayer<DunkkuPlayer>().zoneBiome)
            {
                spawnRangeX = (int)(safeRangeX +10);
                spawnRangeY = (int)(safeRangeY +10);
            }
        }
    }
}
