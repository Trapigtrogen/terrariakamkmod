﻿using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ModLoader;

namespace OfficialKAMKMod
{
    public class DunkkuTile : ModTile
    {
        public override void SetDefaults()
        {
            Main.tileSolid[Type] = true;
            Main.tileMergeDirt[Type] = true;
            Main.tileBlockLight[Type] = true;
            Main.tileLighted[Type] = true;
            //Main.tileDungeon[Type] = true;
            dustType = mod.DustType("Sparkle");
            drop = mod.ItemType("DunkkuTileItem");
            AddMapEntry(new Color(135, 130, 77));
            minPick = 100;
            // Set other values here
        }
    }
}
