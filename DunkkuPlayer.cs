﻿using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using static Terraria.ModLoader.ModContent;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.IO;

namespace OfficialKAMKMod
{
    class DunkkuPlayer : ModPlayer
    {
        public bool zoneBiome = false;

        public override void UpdateBiomes()
        {
            zoneBiome = (DunkkuGen.biomeTiles > 50); // Chance 50 to the minimum number of tiles that need to be counted before it is classed as a biome
        }

        public override bool CustomBiomesMatch(Player other)
        {
            DunkkuPlayer otherPlayer = other.GetModPlayer<DunkkuPlayer>(); // This will get other players using the TutorialPlayerClass
            return zoneBiome == otherPlayer.zoneBiome; // This will return true or false depending on other player
        }

        public override void CopyCustomBiomesTo(Player other)
        {
            DunkkuPlayer otherPlayer = other.GetModPlayer<DunkkuPlayer>();
            otherPlayer.zoneBiome = zoneBiome; // This will set other player's biome to the same as thisPlayer
        }

        public override void SendCustomBiomes(BinaryWriter writer)
        {
            BitsByte flags = new BitsByte();
            flags[0] = zoneBiome;
            writer.Write(flags);
        }

        public override void ReceiveCustomBiomes(BinaryReader reader)
        {
            BitsByte flags = reader.ReadByte();
            zoneBiome = flags[0];
        }

        public override void UpdateBiomeVisuals()
        {

        }

        public override Texture2D GetMapBackgroundImage()
        {
            return null;
        }

        
    }
}
